Maybe you have tried booking a flight online and able to reserve yourself a seat. But have you ever wondered how online flight booking works? If not, this will be your guide to start understanding the process. From here, you can go on for a more complicated information about flight booking.

To see more of online processes, check [this](http://99onlinepoker.net) today.

##Project: Flight Booker


My implementation of the Flight Booker project from the Odin Curriculum.

Project focus: building advanced forms with search and nested attributes.

##Objective
>In this project, you'll get a chance to tackle some more advanced forms. This is the kind of thing you'll have to work with when handling user orders for anything more complicated than an e-book.

Build a flight booker, where user selects flight based on date, departure and arrival airports, and # of passengers.  User enters passenger detail on booking page and receives confirmation.

View: [Demo](http://flight-booker.herokuapp.com)